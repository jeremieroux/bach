
#include "MidiFile.h"
#include <iostream>
#include <map>
#include <sstream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <algorithm>    // std::shuffle
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock

using namespace std;
using namespace smf;

// obtain a time-based seed:
unsigned seed = chrono::system_clock::now().time_since_epoch().count();
default_random_engine e(seed);

vector<int> gammeMajeure = {0,2,4,5,7,9,11};
vector<int> gammeMineure = {0,2,3,5,7,8,11};
vector<int> intervalle = {3,4,5,7};
vector<int> noteSuivante = {-2,2,1,-1,-3,5,3,4,-4,-5,0};
vector<int> noteSuivante2 = {-3,-2,4,-4,1,-1,5,3,-5,2,0};
/*map<float, vector<vector<float>>> rythmeCorr = {{2.5, {{1.,0.5,0.5,0.5},{0.5,0.5,0.5,0.5,0.5}}},
												{2., {{1.,1.},{0.5,0.5,1.},{1.,0.5,0.5}}},
												{1.5, {{1.,0.5},{0.5,0.5,0.5},{0.5,1.}}},
												{1.25, {{0.5,0.5,0.25},{0.25,0.5,0.5},{0.5,.25,0.5}}},
												{1., {{0.5,0.5}}},
												{0.5, {{0.5}}},
												{0.25, {{0.25}}}
												};*/
/*map<vector<float>, vector<float>> contreMesure = {{{2.0,2.0}, {1.,.5,.5,.5,.5,1.}},
											 {{2.0,1.0,1.0}, {1.0,1.0,2.0}},
											 {{1.0,1.0,1.0,1.0}, {2.0,2.0}},
											 {{4.0}, {1.0,1.0,1.0,1.0}},
											 {{2.0,0.5,1.5}, {1.5,0.5,2.0}},
											 {{2.5,0.5,0.5,0.5}, {0.5,0.5,0.5,0.5,2.}}
											 };*/

map<vector<float>, vector<float>> contreMesure = {{{0.5,0.5,0.5,0.5,1.,1.}, {1.,1.5,.5,.5,.5}},
											 {{0.5,0.5,0.5,0.5,2.}, {2.5,.5,.5,.5}},
											 {{0.5,0.5,0.5,0.5,0.5,0.5,1.}, {1.,1.,1.,.5,.5}},
											 {{0.5,0.5,0.5,0.5,0.25,0.25,0.5,1.}, {1.,1.,0.5,0.25,0.25,.5,.5}}
											};

map<string, int> Tnote = {{"C",0},{"C+",1},{"D-",1},{"D",2},{"D+",3},{"E-",3},{"E",4},{"F",5},{"F+",6},{"G-",6},{"G",7},{"G+",8},{"A-",8},{"A",9},{"A+",10},{"B-",10},{"B",11}};
map<int, string> TnomNote = {{0,"Do"},{1,"Do#/Réb"},{2,"Ré"},{3,"Ré#/Mib"},{4,"Mi"},{5,"Fa"},{6,"Fa#/Solb"},{7,"Sol"},{8,"Sol#/Lab"},{9,"La"},{10,"La#/Sib"},{11,"Si"}};
map<string, int> Toctave = {{"0",12},{"1",24},{"2",36},{"3",48},{"4",60},{"5",72},{"6",84},{"7",96}};
map<string, float> Trythme = {{"r",4.0},{"bp",3.0},{"bpp",2.5},{"b",2.0},{"np",1.5},{"n",1.0},{"cp",0.75},{"c",0.5},{"d",0.25},{"tc",0.125}};

struct Note{
	int note;
	float rythme;
};

void afficherNote(Note n, bool retourLigne){
	cout<<TnomNote[n.note%12]<<" "<<(int) (n.note/12)-1<<" | Lg : "<<n.rythme;
	if(retourLigne){
		cout<<endl;
	}
}

void afficherListe(vector<float> L){
	cout<<"{";
	for(int i=0; i<L.size(); i++){
		cout<<L[i];
		if(i!=L.size()-1){
			cout<<", ";
		}
	}
	cout<<"}";
}

void afficherListeNote(vector<Note> n){
	cout<<"{";
	for(int i=0; i<n.size(); i++){
		afficherNote(n[i],false);
		if(i!=n.size()-1){
			cout<<", ";
		}
	}
	cout<<"}"<<endl;
}

float longueur(vector<Note> melodie){
	float result=0.;
	for(unsigned int i=0; i<melodie.size(); i++){
		result+=melodie[i].rythme;
	}
	return result;
}

Note getNote(vector<Note> melodie, float temps){
	float t=1.;
	int i=0;
	while(t<temps){
		t+=melodie[i].rythme;
		i++;
	}
	if(temps!=t){
		i--;
	}
	cout<<"T = "<<temps<<" -> "<<TnomNote[melodie[i].note%12]<<" "<<(int) (melodie[i].note/12)-1<<endl;
	return melodie[i];

}

void afficherNote(vector<Note> melodie, float temps){
	afficherNote(getNote(melodie,temps),true);
}

string toString(char c){
	string s = "";
	return s+=c;
}

string toString(char c1, char c2){
	string s = "";
	s+=c1;
	return s+=c2;
}
string toString(char c1, char c2, char c3){
	string s = "";
	s+=c1;
	s+=c2;
	return s+=c3;
}

float toFloat(char c, char d, char u){
	// Rythme chiffre/nombre
	float c1 = c - '0';
	float d1 = d - '0';
	float u1 = u - '0';
	return c1*100. + d1*10. + u1;
}

vector<Note> lire(string s){
	vector<Note> musique;
	size_t pos = 0;
	s+=" ";
	string note;
	while ((pos = s.find(" ")) != string::npos) {
		note = s.substr(0, pos);
		Note n;
	
		if(note[0]=='_'){
			n.note=-1;
		}
		else if(note[2]=='_'){
			n.note=Tnote[toString(note[0])]+Toctave[toString(note[1])];
		}
		else if(note[3]=='_'){
			n.note=Tnote[toString(note[0],note[1])] + Toctave[toString(note[2])];
		}
		else{
			cout << "Note non reconnue : "<<note << endl;
		}

		if(note[note.size()-2]=='_'){
			if(note[note.size()-1]>';'){
				n.rythme=Trythme[toString(note[note.size()-1])];
			}
			else{
				n.rythme=toFloat('0', '0', note[note.size()-1]);
			}
		}
		else if(note[note.size()-3]=='_'){ 
			if(note[note.size()-1]>';'){
				n.rythme=Trythme[toString(note[note.size()-2], note[note.size()-1])];
			}
			else{
				n.rythme=toFloat('0', note[note.size()-2], note[note.size()-1]);
			}
		}
		else if(note[note.size()-4]=='_'){
			if(note[note.size()-1]>';'){
				n.rythme=Trythme[toString(note[note.size()-3], note[note.size()-2], note[note.size()-1])];
			}
			else{
				n.rythme=toFloat(note[note.size()-3], note[note.size()-2], note[note.size()-1]);
			}
		}
		else{
			cout << "Rythme non reconnu : "<<note << endl;
		}
		//cout<<"Ecrire note : "<< note << endl;
		musique.push_back(n);
		s.erase(0, pos+1);

	}
	return musique;
}

vector<Note> transposition(vector<Note> melodie, int ecart){
	vector<Note> transposition;
	for(unsigned int i=0; i<melodie.size(); i++){
		Note n;
		if(melodie[i].note==-1){
			n.note=-1;
		}
		else{
			n.note=melodie[i].note + ecart;
		}
		n.rythme=melodie[i].rythme;
		transposition.push_back(n);
	}
	return transposition;
}

vector<int> getTonalite(string t){
	if(t=="m"){
		return gammeMineure;
	}
	else if(t=="M"){
		return gammeMajeure;
	}
	else{
		cout<<"Tonalité non reconnue";
		vector<int> vide;
		return vide;
	}
}

void ecrire(MidiFile& outputfile, vector<uchar>& midievent, vector<Note> melodie, int tpq, int track){
	int actiontime = 0;      // temporary storage for MIDI event time
	midievent[2] = 64;       // store attack/release velocity for note command
	for(unsigned int i=0; i<melodie.size(); i++){
		if(melodie[i].note==-1){
			actiontime += tpq * melodie[i].rythme;
		}
		else{
			midievent[0] = 0x90;     // store a note on command (MIDI channel 1)
			midievent[1] = melodie[i].note;
			outputfile.addEvent(track, actiontime, midievent);
			actiontime += tpq * melodie[i].rythme;
			midievent[0] = 0x80;     // store a note on command (MIDI channel 1)
			outputfile.addEvent(track, actiontime, midievent);
		}
	}
}

void decalage(vector<Note>& melodie, float decalage){

	// Ecriture du décalage dans la table
	vector<Note> melodie2;
	Note n;
	n.note=-1;
	n.rythme=(float) decalage;
	melodie2.push_back(n);

	for(unsigned int i=0; i<melodie.size(); i++){
		melodie2.push_back(melodie[i]);
	}

	// Ecriture du décalage dans la table
	melodie=melodie2;
}

Note newNote(int h, float l){
	Note n;
	n.note=h;
	n.rythme=l;
	return n;
}

template <typename T>
T random(vector<T> v){;
	return v[(rand()%v.size())];
}

vector<vector<float>> random(map<vector<float>, vector<float>> m){
	vector<vector<float>> result;
	int nb = rand()%m.size();
	int i=0;
	for (auto const& pair: m){
		if(i==nb){
			result.push_back(pair.first);
			result.push_back(pair.second);
		}
		i++;
    }
	return result;
}

bool inGamme(int ref, int gamme, vector<int> tonalite){
	for(unsigned int i=0; i<tonalite.size(); i++){
		if((gamme+tonalite[i])%12==ref%12){
			return true;
		}
	}
	return false;
}

int choixDebutPhraseMarche(int ref, int gamme, vector<int> tonalite, int sens, vector<int> dec){
	for(int i=0; i<dec.size(); i++){
		if(inGamme(ref+dec[i]*sens,gamme,tonalite)){
			return dec[i]*sens;
		}
	}
	cout<<"Problème décalage marche harmonique"<<endl;
	return 0;
}

bool inIntervalle(int ref){
	for(unsigned int i=0; i<intervalle.size(); i++){
		if(intervalle[i]==ref || intervalle[i]==-ref){
			return true;
		}
	}
	return false;
}

int noteHarmonique(int ref, vector<int> tonalite, int gamme, int pred){
	for(unsigned int i=0; i<noteSuivante.size(); i++){
		if(ref>pred+noteSuivante[i] && inIntervalle(ref-(pred+noteSuivante[i])) && inGamme(pred+noteSuivante[i],gamme,tonalite)){
			return pred+noteSuivante[i];
		}
	}
	return ref;
}

int noteApres(vector<int> tonalite, int gamme, int pred){
	for(unsigned int i=0; i<noteSuivante.size(); i++){
		if(inGamme(pred+noteSuivante[i],gamme,tonalite)){
			return pred+noteSuivante[i];
		}
	}
	return pred;
}

template <typename T>
vector<T> melanger(vector<T> v){
	shuffle(v.begin(), v.end(), e);
	return v;
}

vector<float> getRythmeMesure(vector<Note> v){
	vector<float> result;
	for(int i=0; i<v.size(); i++){
		result.push_back(v[i].rythme);
	}
	return result;
}

vector<Note> getContreMesure(vector<Note> v, float debutMesure){
	vector<Note> mesureLue;
	float tActuel=debutMesure;
	if((int) tActuel%4!=1){
		cout<<"Vous n'êtes pas au début de la mesure (t="<<tActuel<<")"<<endl;
	}
	while(tActuel<debutMesure+4.){
		Note n = getNote(v,tActuel);
		mesureLue.push_back(n);
		tActuel+=n.rythme;
	}
	afficherListeNote(mesureLue);
	vector<float> rythmeMesureLue = getRythmeMesure(mesureLue);
	vector<float> rythmeContreMesure = contreMesure[rythmeMesureLue];
	afficherListe(rythmeMesureLue);
	cout<<" -> ";
	afficherListe(rythmeContreMesure);
	cout<<endl;

	tActuel=debutMesure;
	vector<Note> mesureEnvoyee;
	int i=0;
	while(tActuel<debutMesure+4.){
		Note n = getNote(v,tActuel);
		n.rythme = rythmeContreMesure[i];
		mesureEnvoyee.push_back(n);
		tActuel+=rythmeContreMesure[i];
		i++;
	}
	afficherListeNote(mesureEnvoyee);
	cout<<endl;
	return mesureEnvoyee;
}

void contreSujet(vector<Note>& v1, float l1, vector<Note> v2, float l2, vector<int> tonalite, int gamme){
	l1+=1.;
	l2+=1.;
	while(l1<l2){
		vector<Note> contreM = getContreMesure(v2,l1);
		for(int i=0; i<contreM.size(); i++){
			v1.push_back(newNote(noteHarmonique(contreM[i].note,tonalite,gamme,v1[v1.size()-1].note),contreM[i].rythme));
			l1+=contreM[i].rythme;
		}
	}
}

void marcheHarmonique(vector<Note>& v1, float l1, vector<Note>& v2, float l2, vector<int> tonalite, int gamme){
	map<vector<float>, vector<float>> motifMarcheHarmonique = {
																{{0.5,0.5,0.5,0.5,2.}, {1.,1.5,.5,.5,.5}}
															};
	vector<vector<float>> rythme = random(motifMarcheHarmonique);

	// Choix de la voix pour le début de la marche harmonique
	int choixVoix = rand()%2;
	vector<float> rythmeVoix1 = rythme[choixVoix];
	vector<float> rythmeVoix2 = rythme[1-choixVoix];

	afficherListe(rythmeVoix1);
	afficherListe(rythmeVoix2);

	for(int i=0; i<rythmeVoix1.size(); i++){
		//Note n = getNote(v2,l1);
		//v1.push_back(newNote(noteHarmonique(n.note,tonalite,gamme,v1[v1.size()-1].note),rythmeVoix1[i]));
		if(i==0){
			noteSuivante = melanger(noteSuivante);
			v1.push_back(newNote(noteApres(tonalite,gamme,getNote(v1,l1).note),rythmeVoix1[i]));
			noteSuivante=noteSuivante2;
		}
		else{
			v1.push_back(newNote(noteApres(tonalite,gamme,getNote(v1,l1).note),rythmeVoix1[i]));
		}
		l1+=rythmeVoix1[i];
	}	
	for(int i=0; i<rythmeVoix2.size(); i++){
		Note n = getNote(v1,l2);
		v2.push_back(newNote(noteHarmonique(n.note,tonalite,gamme,v2[v2.size()-1].note),rythmeVoix2[i]));
		l2+=rythmeVoix2[i];
	}										
	//v1.push_back(newNote(noteHarmonique(n.note,tonalite,gamme,v1[v1.size()-1].note),n.rythme));

	l1+=1.;
	l2+=1.;
	// Suite de la marche harmonique (copie et décalage de la première)
	int sensProgression = rand()%2;
	if(sensProgression==0){
		sensProgression=-1; // -1 descente, +1 montée
	}

	int nbMarche = (rand()%2)+3;
	cout<<"NB : "<<nbMarche<<endl;
	vector<int> intervalleMarche = {1,2};
	Note ref = getNote(v1,l1-4.);

	for(int i=0; i<nbMarche; i++){
		int dec = choixDebutPhraseMarche(ref.note,gamme,tonalite,sensProgression,intervalleMarche);
		ref.note+=dec;
		
		// Voix 1
		float sum=1.;
		while(sum<4.){
			Note n = getNote(v1,l1-4.);
			n.note+=dec; 
			v1.push_back(n);
			l1+=n.rythme;
			sum+=n.rythme;
		}

		// Voix 2
		sum=1.;
		while(sum<4.){
			Note n2 = getNote(v2,l2-4.);
			n2.note+=dec; 
			v2.push_back(n2);
			l2+=n2.rythme;
			sum+=n2.rythme;
		}
	}
}

///////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {
	
	/* initialize random seed: */
  	srand (time(NULL));

	MidiFile outputfile;        // create an empty MIDI file with one track
	outputfile.absoluteTicks();  // time information stored as absolute time
	                           // (will be coverted to delta time when written)
	outputfile.addTrack(2);     // Add another two tracks to the MIDI file
	vector<uchar> midievent;     // temporary storage for MIDI events
	midievent.resize(3);        // set the size of the array to 3 bytes
	int tpq = 48;              // default value in MIDI file is 48
	outputfile.setTicksPerQuarterNote(tpq);

	string gamme="C";
	string ton="M";
	vector<int> tonalite=getTonalite(ton);
	//vector<Note> sujet = lire("D4_b A4_b F4_b D4_b C+4_b D4_n E4_n F4_bpp G4_c F4_c E4_c");
	//vector<Note> sujet = lire("C4_c C4_c C4_c D4_c E4_n D4_n C4_c E4_c D4_c D4_c C4_b");
	vector<Note> sujet = lire("C4_c C4_c G4_c G4_c A4_c A4_c G4_n F4_c F4_c E4_c E4_c D4_d C4_d D4_c C4_n");
	
	int transpo=7; // Nombre de demi tons pour la réexposition
	vector<Note> sujetTranspose = transposition(sujet,transpo);
	float lg_Sujet = longueur(sujet);

	// Exposition du sujet sur la voix 2 après exposition du sujet sur la voix 1
	decalage(sujetTranspose,lg_Sujet);
	
	// Ecrire le contre sujet sur la voix 1 le temps de l'exposition du sujet sur la voix 2
	contreSujet(sujet,lg_Sujet,sujetTranspose,lg_Sujet*2,tonalite,Tnote[gamme]+transpo);

	cout<<"-----------------"<<endl;

	// Ecrire la suite
	lg_Sujet = longueur(sujet);
	marcheHarmonique(sujet,lg_Sujet,sujetTranspose,lg_Sujet,tonalite,Tnote[gamme]+transpo);

	ecrire(outputfile,midievent,sujet,tpq,2); // Voix 1
	ecrire(outputfile,midievent,sujetTranspose,tpq,1); // Voix 2
	outputfile.sortTracks();         // make sure data is in correct order
	outputfile.write("musique/fugue.mid");
	return 0;
}


/*  FUNCTIONS available in the MidiFile class:

void absoluteTime(void);
   Set the time information to absolute time.
int addEvent(int aTrack, int aTime, vector<uchar>& midiData);
   Add an event to the end of a MIDI track.
int addTrack(void);
   Add an empty track to the MIDI file.
int addTrack(int count);
   Add a number of empty tracks to the MIDI file.
void deltaTime(void);
   Set the time information to delta time.
void deleteTrack(int aTrack);
   remove a track from the MIDI file.
void erase(void);
   Empty the contents of the MIDI file, leaving one track with no data.
MFEvent& getEvent(int aTrack, int anIndex);
   Return a MIDI event from the Track.
int getTimeState(void);
   Indicates if the timestate is TIME_STATE_ABSOLUTE or TIME_STATE_DELTA.
int getTrackState(void);
   Indicates if the tracks are being processed as multiple tracks or
   as a single track.
int getTicksPerQuarterNote(void);
   Returns the ticks per quarter note value from the MIDI file.
int getTrackCount(void);
   Returns the number of tracks in the MIDI file.
int getNumTracks(void);
   Alias for getTrackCount();
int getNumEvents(int aTrack);
   Return the number of events present in the given track.
void joinTracks(void);
   Merge all tracks together into one track.  This function is reversable,
   unlike mergeTracks().
void mergeTracks(int aTrack1, int aTrack2);
   Combine the two tracks into a single track stored in Track1.  Track2
   is deleted.
int read(char* aFile);
   Read the contents of a MIDI file into the MidiFile class data structure
void setTicksPerQuarterNote    (int ticks);
   Set the MIDI file's ticks per quarter note information
void sortTrack(vector<MFEvent>& trackData);
   If in absolute time, sort particular track into correct time order.
void sortTracks(void);
   If in absolute time, sort tracks into correct time order.

*/