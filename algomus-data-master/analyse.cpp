#include <fstream>
#include <string>
#include <iomanip> // std::setw pour un affichage propre
#include <vector>
#include <map>
#include <iostream>
#include "json.hpp" // Json
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

using namespace std;
using namespace nlohmann;

map<int, vector<int>> voixEntree = {{1, {1,2}},
								{8, {1,2,3}},
								{12, {2,1,3}},
								{14, {2,1,4,3}},
								{16, {3,2,1,4}},
								{18, {3,2,4,1}},
								{19, {2,1,3,4}},
								{20, {2,3,4,1}},
								{21, {3,4,1,2}},
								{22, {4,3,2,1}},
								{23, {1,2,3,4,5}},
								{24, {5,4,3,2,1}}
								};

// Chemin des fichiers JSON dans lesquels on trouve les éléments des fugues
string cheminFichier(int i){
	string result = "fugues/bach-wtc-i/";
	if(i<10){
		result+="0";
	}
	result+=to_string(i)+"-bwv"+to_string(845+i)+"-ref.dez";
	return result;
}

// Calcul du nombre de voix dans une fugue
int nbVoix(json j){
	int result=0;
	for(unsigned int i=0; i<j["labels"].size(); i++){
		if(j["labels"][i].contains("staff")){
			int valeur = std::stoi((string) j["labels"][i]["staff"]);
  			if(valeur>result){
  				result=valeur;
  			}
		}
	}
	return result;
}

// Statistiques liées au nombre de voix par fugue
void statNbVoix(json data){
	for(unsigned int i=1; i<data.size(); i++){
		if(!data[i]["nbVoix"].is_null()){
			cout<<i<<" -> "<<data[i]["nbVoix"]<<endl;
		}
	}
}

// Ordonner selon le début de l'élément
json ordonner(json data, int i){
	json tri;
	while(data[i]["labels"].size()!=0){
		float min=data[i]["labels"][0]["start"];
		int idMin=0;
		int nb=0;
		for(unsigned int j=0; j<data[i]["labels"].size(); j++){
			float temp = data[i]["labels"][j]["start"];
			if(temp<min){
				min=temp;
				idMin=j;
			}
		}
		//cout<<data[i]["labels"][idMin]<<endl;
		tri.push_back(data[i]["labels"][idMin]);
		data[i]["labels"].erase(idMin);
		nb++;
	}
	// cout<<tri<<endl;
	return tri;
}

// Remplacer les éléments par les éléments triés
json remplacer(json data){
	for(unsigned int i=1; i<data.size(); i++){
		data[i]["labels"]=ordonner(data,i);
	}
	return data;
}

// Afficher le déroulé d'une fugue
void afficherDeroule(json data, int i){
	for(unsigned int j=0; j<data[i]["labels"].size(); j++){
		float debut = data[i]["labels"][j]["start"];
		string nom = data[i]["labels"][j]["type"];
		string voix = "*";
		if(!data[i]["labels"][j]["staff"].is_null()){
			voix = data[i]["labels"][j]["staff"];
		}
		cout<<"("<<debut<<" "<<nom<<" "<<voix<<") ";
	}
	cout<<endl;
}

// Afficher des statistiques sur la voix de début selon le nombre de voix
void statProbaVoixDebut(json data, int nbVoix){
	for(unsigned int i=1; i<data.size(); i++){
		if(data[i]["nbVoix"]==nbVoix){
			string voix = data[i]["labels"][0]["staff"];
			cout<<voix<<" ";
		}
	}
	cout<<endl;
}

// Tableau qui est le schéma de la fugue
vector<vector<string> > tableauFugue(json data, int idFugue){
	int nbVoix = data[idFugue]["nbVoix"];
	// Initialisation
	vector<vector<string> > T;
	for(int i=0; i<=nbVoix; i++){
		vector<string> voix;
		if(i==0){
			voix.push_back("Fugue"+to_string(idFugue));
		}
		else{
			voix.push_back(to_string(i));
		}
		T.push_back(voix);
	}
	float longueurRefSujet=(float) data[idFugue]["labels"][0]["duration"]/2.;
	for(unsigned int j=0; j<data[idFugue]["labels"].size(); j++){
		if(!data[idFugue]["labels"][j]["staff"].is_null()){
			int voix = std::stoi((string) data[idFugue]["labels"][j]["staff"]);
			// Si une autre structure commence dans la mesure
			if(j!=0 && ((float) data[idFugue]["labels"][j]["start"])-((float) data[idFugue]["labels"][j-1]["start"])<longueurRefSujet){
				T[voix][T[voix].size()-1]=data[idFugue]["labels"][j]["type"];
			}
			else{
				T[voix].push_back(data[idFugue]["labels"][j]["type"]);
				T[0].push_back(to_string(data[idFugue]["labels"][j]["start"]));
				for(int i=1; i<=nbVoix; i++){
					if(i!=voix){
						if(T[i].size()==1 || T[i].back()==""){
							T[i].push_back("");
						}
						else{
							T[i].push_back("?");
						}
					}
				}
			}
		}
		else{
			//bool decalerCompteMesure=true;
			T[0].push_back(to_string(data[idFugue]["labels"][j]["start"]));
			for(int i=1; i<=nbVoix; i++){
				if(data[idFugue]["labels"][j]["type"]=="Structure"){
					/*if(j!=0 && ((float) data[idFugue]["labels"][j]["start"])-((float) data[idFugue]["labels"][j-1]["start"])<longueurRefSujet){
						T[i][T[i].size()-1]=data[idFugue]["labels"][j]["tag"];
						decalerCompteMesure=false;
					}
					else{*/
						T[i].push_back(data[idFugue]["labels"][j]["tag"]);
					//}
				}
				else{
					/*if(j!=0 && ((float) data[idFugue]["labels"][j]["start"])-((float) data[idFugue]["labels"][j-1]["start"])<longueurRefSujet){
						T[i][T[i].size()-1]=data[idFugue]["labels"][j]["type"];
						decalerCompteMesure=false;
					}
					else{*/
						T[i].push_back(data[idFugue]["labels"][j]["type"]);
					//}
				}
			}
			/*if(decalerCompteMesure){
				T[0].push_back(to_string(data[idFugue]["labels"][j]["start"]));
			}*/
		}
	}

	// Nettoyage
	for(unsigned int i=0; i<T.size(); i++){
		for(unsigned int j=0; j<T[i].size(); j++){
			if(T[i][j]=="Cadence" || T[i][j]=="cad:PAC ?"){
				T[i][j]="Cad.";
			}
			else if(T[i][j]=="Pedal"){
				T[i][j]="Ped.";
			}
			else if(T[i][j]=="S-invinc" || T[i][j]=="S-invinccc"){
				T[i][j]="S-invc";
			}
			else if(T[i][j]=="ignore"){
				T[i][j]="Var.";
			}
			else if(T[i][j]=="episode"){
				T[i][j]="Epi.";
			}
			else if(T[i][j]=="codetta"){
				T[i][j]="Cod.";
			}
			else if(T[i][j]=="CS1-in-episode"){
				T[i][j]="CS1-Ep.";
			}
			else if(T[i][j]==""){
				T[i][j]="vide";
			}
		}
	}

	return T;

}

// Afficher le tableau de la fugue 
void afficherFugue(vector<vector<string> > T){
	for(unsigned int i=0; i<T.size(); i++){
		for(unsigned int j=0; j<T[i].size(); j++){
			cout<<setw(4)<<T[i][j]<<" ";
		}
		cout<<endl;
	}
	cout<<endl;
}

// Afficher toutes les fugues
void afficherFugues(json data){
	for(int i=1; i<=24; i++){
		vector<vector<string> > tabFugue = tableauFugue(data,i);
		afficherFugue(tabFugue);
		cout<<endl<<endl;
	}
}

// Afficher les listes d'éléments de fugues possibles
void afficherElemFugue(json data){
	vector<string> L;
	for(unsigned int i=1; i<=24; i++){
		vector<vector<string> > tabFugue = tableauFugue(data,i);
		for(unsigned int j=1; j<tabFugue.size(); j++){
			for(unsigned int k=1; k<tabFugue[j].size(); k++){
				if(find(L.begin(), L.end(), tabFugue[j][k]) == L.end()) {
    				L.push_back(tabFugue[j][k]);
    				cout<<"'"<<tabFugue[j][k]<<"'"<<endl;
				}
			}
		}
	}
	cout<<endl;
}

// Statistiques de nombre de transition
map<string,int> statNbTransition(json data, string e){
	// Initialisation
	ifstream fichier("listeElem.txt");
	map<string,int> M;
	string ligne;
	if(fichier){
        while(getline(fichier, ligne)){
        	M.insert(std::pair<string,int>(ligne,0));
       	}
    }

    // Remplissage
	for(int i=1; i<=24; i++){
		vector<vector<string> > tabFugue = tableauFugue(data,i);
		for(unsigned int j=1; j<tabFugue.size(); j++){
			for(unsigned int k=1; k<tabFugue[j].size(); k++){
				if(tabFugue[j][k]==e){
					if(k==tabFugue[j].size()-1){
						M["FIN"]++;
					}
					else{
						M[tabFugue[j][k+1]]++;
					}
					M["TOTAL"]++;
				}
			}
		}
	}
	return M;
}

// Affichage du nombre de transitions
void affichageNbTransition(json data){
	// Première ligne
	cout<<"\\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}"<<endl;
	cout<<"\\hline"<<endl<<"$\\nearrow$ & ";
	ifstream fichier0("listeElem.txt");
    string ligne0;
    if(fichier0){
        while(getline(fichier0, ligne0)){
        	if(ligne0=="TOTAL"){
			    cout<<"TOTAL \\\\"<<endl<<"\\hline"<<endl;
			}
			else{
			    cout<<ligne0<<" & ";
			}
       	}
    }

    // Suite
	ifstream fichier1("listeElem.txt");
    string ligne1;
	if(fichier1){
        while(getline(fichier1, ligne1)){
        	if(ligne1!="FIN" && ligne1!="TOTAL"){
        		map<string,int> M = statNbTransition(data, ligne1);
				cout<<ligne1<<" & ";
				
				// Affichage LaTeX
			    ifstream fichier2("listeElem.txt");
			    string ligne2;
				if(fichier2){
			        while(getline(fichier2, ligne2)){
			        	if(M[ligne2]!=0){
			        			cout<<M[ligne2];
			        		}
			        	if(ligne2=="TOTAL"){
			        		cout<<" \\\\"<<endl<<"\\hline"<<endl;
			        	}
			        	else{
			        		cout<<" & ";
			        	}
			       	}
			    }
        	}
        }
    }
    cout<<"\\end{tabular}"<<endl<<endl;
}

// Affichage des probas de transitions
void affichageProbaTransition(json data){
	// Première ligne
	cout<<"\\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}"<<endl;
	cout<<"\\hline"<<endl<<"$\\nearrow$ & ";
	ifstream fichier0("listeElem.txt");
    string ligne0;
    if(fichier0){
        while(getline(fichier0, ligne0)){
        	if(ligne0!="TOTAL"){
	        	if(ligne0=="FIN"){
				    cout<<"FIN \\\\"<<endl<<"\\hline"<<endl;
				}
				else{
				    cout<<ligne0<<" & ";
				}
			}
       	}
    }

    // Suite
	ifstream fichier1("listeElem.txt");
    string ligne1;
	if(fichier1){
        while(getline(fichier1, ligne1)){
        	if(ligne1!="FIN" && ligne1!="TOTAL"){
        		map<string,int> M = statNbTransition(data, ligne1);
				cout<<ligne1<<" & ";
				
				// Affichage LaTeX
			    ifstream fichier2("listeElem.txt");
			    string ligne2;
				if(fichier2){
			        while(getline(fichier2, ligne2)){
			        	if(ligne2!="TOTAL"){
			        		if(M[ligne2]!=0){
			        				float result = ((int) ((float)M[ligne2]/M["TOTAL"] * 1000))/ 1000.;
				        			cout<<result;
				        			// Coloration des cases
				        			if(result>=0.15){
				        				cout<<"\\cellcolor{blue!50}";
				        			}

				        		}
				        	if(ligne2=="FIN"){
				        		cout<<" \\\\"<<endl<<"\\hline"<<endl;
				        	}
				        	else{
				        		cout<<" & ";
				        	}
			        	}
			       	}
			    }
        	}
        }
    }
    cout<<"\\end{tabular}"<<endl<<endl;
}

// Statistiques de nombre de superposition
map<string,int> statNbSuperposition(json data, string e){
	// Initialisation
	ifstream fichier("listeElem.txt");
	map<string,int> M;
	string ligne;
	if(fichier){
        while(getline(fichier, ligne)){
        	M.insert(std::pair<string,int>(ligne,0));
       	}
    }

    // Remplissage
	for(int i=1; i<=24; i++){
		vector<vector<string> > tabFugue = tableauFugue(data,i);
		for(unsigned int j=1; j<tabFugue.size(); j++){
			for(unsigned int k=1; k<tabFugue[j].size(); k++){
				for(unsigned int l=1; l<tabFugue.size(); l++){
					if(tabFugue[j][k]==e && j!=l){
						M[tabFugue[l][k]]++;
						M["TOTAL"]++;
					}
				}
			}
		}
	}
	return M;
}

// Affichage du nombre de superpositions
void affichageNbSuperposition(json data){
	// Première ligne
	cout<<"\\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}"<<endl;
	cout<<"\\hline"<<endl<<"$\\nearrow$ & ";
	ifstream fichier0("listeElem.txt");
    string ligne0;
    if(fichier0){
        while(getline(fichier0, ligne0)){
        	if(ligne0!="FIN"){
	        	if(ligne0=="TOTAL"){
				    cout<<"TOTAL \\\\"<<endl<<"\\hline"<<endl;
				}
				else{
				    cout<<ligne0<<" & ";
				}
			}
       	}
    }

    // Suite
	ifstream fichier1("listeElem.txt");
    string ligne1;
	if(fichier1){
        while(getline(fichier1, ligne1)){
        	if(ligne1!="FIN" && ligne1!="TOTAL"){
        		map<string,int> M = statNbSuperposition(data, ligne1);
				cout<<ligne1<<" & ";
				
				// Affichage LaTeX
			    ifstream fichier2("listeElem.txt");
			    string ligne2;
				if(fichier2){
			        while(getline(fichier2, ligne2)){
			        	if(ligne2!="FIN"){
				        	if(M[ligne2]!=0){
				        		cout<<M[ligne2];
				        	}
				        	if(ligne2=="TOTAL"){
				        		cout<<" \\\\"<<endl<<"\\hline"<<endl;
				        	}
				        	else{
				        		cout<<" & ";
				        	}
				        }
			       	}
			    }
        	}
        }
    }
    cout<<"\\end{tabular}"<<endl<<endl;
}

// Affichage des probas de transitions
void affichageProbaSuperposition(json data){
	// Première ligne
	cout<<"\\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}"<<endl;
	cout<<"\\hline"<<endl<<"$\\nearrow$ & ";
	ifstream fichier0("listeElem.txt");
    string ligne0;
    if(fichier0){
        while(getline(fichier0, ligne0)){
        	if(ligne0!="TOTAL" && ligne0!="FIN"){
	        	if(ligne0=="?"){
				    cout<<"? \\\\"<<endl<<"\\hline"<<endl;
				}
				else{
				    cout<<ligne0<<" & ";
				}
			}
       	}
    }

    // Suite
	ifstream fichier1("listeElem.txt");
    string ligne1;
	if(fichier1){
        while(getline(fichier1, ligne1)){
        	if(ligne1!="FIN" && ligne1!="TOTAL"){
        		map<string,int> M = statNbSuperposition(data, ligne1);
				cout<<ligne1<<" & ";
				
				// Affichage LaTeX
			    ifstream fichier2("listeElem.txt");
			    string ligne2;
				if(fichier2){
			        while(getline(fichier2, ligne2)){
			        	if(ligne2!="TOTAL" && ligne2!="FIN"){
			        		if(M[ligne2]!=0){
		        				float result = ((int) ((float)M[ligne2]/M["TOTAL"] * 1000))/ 1000.;
			        			cout<<result;
			        			// Coloration des cases
			        			if(result>=0.15){
			        				cout<<"\\cellcolor{blue!50}";
			        			}

			        		}
				        	if(ligne2=="?"){
				        		cout<<" \\\\"<<endl<<"\\hline"<<endl;
				        	}
				        	else{
				        		cout<<" & ";
				        	}
			        	}
			       	}
			    }
        	}
        }
    }
    cout<<"\\end{tabular}"<<endl<<endl;
}

vector<int> choixEntreeSujet(){
	srand(time(NULL));
	int id = rand()%24 + 1;
	while(voixEntree[id].size()==0){
		id++;
	}
	return voixEntree[id];
}

void afficherListe(vector<int> L){
	cout<<endl<<"LISTE : ";
	for(unsigned int i=0; i<L.size(); i++){
		cout<<L[i]<<" ";
	}
	cout<<endl<<endl;
}

string suite(string e, vector<string> elem, vector<vector<float> > trans){
	int id=0;
	while(id<(int)elem.size() && elem[id]!=e){
		id++;
	}
	if(id==(int)elem.size()){
		cout<<"Mauvais nom d'élément : "<<e<<endl;
		return "";
	}
	else{
		float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		float sum=0.;
		int i=0;
		while(sum<r){
			sum+=trans[id][i];
			i++;
		}
		//cout<<"r = "<<r<<" | elem = "<<elem[i-1]<<endl;
		return elem[i-1];
	}
}

vector<vector<string> > fugue(vector<int> ordre, vector<string> elem, vector<vector<float> > trans, int longueurMin){
	afficherListe(ordre);
	int idEntreeSujet=0;
	vector<vector<string> > F;
	// Première ligne
	/*vector<string> ligne0;
	ligne0.push_back("TEST");
	for(unsigned int i=0; i<=ordre.size(); i++){
		if(i==0){
			ligne0.push_back("0");
		}
		else if(i==1){
			ligne0.push_back("x");
		}
		else{
			ligne0.push_back(to_string(i)+"x");
		}
	}
	F.push_back(ligne0);*/

	// Initialisation
	for(int i=1; i<=(int)ordre.size(); i++){
		vector<string> ligne;
		int id=0;
		while(ordre[id]!=i){
			id++;
		}
		ligne.push_back(to_string(i));
		F.push_back(ligne);
	}

	// Remplissage étagement
	while(idEntreeSujet<(int)ordre.size()){
		for(int i=0; i<(int)ordre.size(); i++){
			if(i==idEntreeSujet){
				F[ordre[idEntreeSujet]-1].push_back("S");
			}
			else if(i<idEntreeSujet){
				F[ordre[idEntreeSujet]-1].push_back("vide");
			}
			else{
				if(F[ordre[idEntreeSujet]-1].back()=="Cad"){
					F[ordre[idEntreeSujet]-1].push_back("Var");
				}
				else{
					F[ordre[idEntreeSujet]-1].push_back(suite(F[ordre[idEntreeSujet]-1].back(),elem,trans));
				}	
			}
		}
		idEntreeSujet++;
	}

	int lg=idEntreeSujet;
	// Suite
	while(F[0].back()!="FIN"){
		// Sélection aléatoire de la voix de la première transition
		int voixTrans = rand() % (int)ordre.size();
		cout<<1<<endl;
		string suivant;
		if(F[voixTrans].back()=="FIN"){
			suivant = "S";
		}
		else{
			suivant = suite(F[voixTrans].back(),elem,trans);
		}
		bool cadence = false;
		if(suivant=="Cad"){
			lg++;
			for(int i=0; i<(int)ordre.size(); i++){
				F[i].push_back("Cad");
			}
			if(lg>=longueurMin){
				cout<<2<<endl;
				if(suite(F[0].back(),elem,trans)=="Ped"){
					for(int i=0; i<(int)ordre.size(); i++){
						F[i].push_back("Ped");
					}
				}
				for(int i=0; i<(int)ordre.size(); i++){
					F[i].push_back("FIN");
				}
				return F;
			}
			else{
				cadence = true;
			}
		}
		if(suivant=="Var"){
			lg++;
			int nbVar = rand()%2;
			for(int i=0; i<(int)ordre.size(); i++){
				F[i].push_back("Var");
			}
			if((int)ordre.size()!=2 || (int)ordre.size()==nbVar){
				int voix=rand() % (int)ordre.size();
				cout<<3<<endl;
				suivant = suite(F[voix].back(),elem,trans);
				F[voix][F[voix].size()-1]=suivant;
			}
		}
		else{
			lg++;
			if(cadence){
				for(int i=0; i<(int)ordre.size(); i++){
					/*if(i==voixTrans){
						F[i].push_back("S");
					}
					else{
						cout<<4<<endl;
						suivant = suite(F[i][(int)F[i].size()-2],elem,trans);
					}*/
					if(F[i][(int)F[i].size()-2]=="Cad"){
						F[i].push_back("S");
					}
					else{
						suivant = suite(F[i][(int)F[i].size()-2],elem,trans);
						if(suivant=="Cad" || suivant=="Ped"){
							//suivant = suite(F[i][(int)F[i].size()-2],elem,trans);
							F[i].push_back("Var");
						}
						else{
							F[i].push_back(suivant);
						}
					}
				}
			}
			else{
				for(int i=0; i<(int)ordre.size(); i++){
					cout<<5<<" "<<F[i].back()<<endl;
					if(F[i].back()=="FIN"){
						F[i][F[i].size()-1]="Var";
					}
					suivant = suite(F[i].back(),elem,trans);
					if(suivant=="Cad" || suivant=="Ped"){
						int type=rand() % 2;
						if(type==0){
							F[i].push_back("S");
						}
						else{
							F[i].push_back("Var");
						}
					}
					else{
						F[i].push_back(suivant);
					}
				}
			}

		}
	}
	for(int i=0; i<(int)ordre.size(); i++){
		F[i][F[i].size()-1]=="Cad";
		F[i].push_back("Ped");
		F[i].push_back("FIN");
	}
	return F;
}

// Main
int main(int argc, char** argv)
{
	srand(time(NULL));
	if(argc!=1){
		cout<<"ERREUR : Il faut 0 argument !"<<endl;
	}
	else{
		// json data;
		// for(int i=1; i<=24; i++){
		// 	ifstream fichier(cheminFichier(i));
		// 	json j;
		// 	fichier>>j;
		// 	j.erase("meta"); // Supprimer un élément inutile
		// 	j["nbVoix"]=nbVoix(j); // Ajouter un compteur de nombre de voix
		// 	//cout<<setw(4)<<j<<endl;
		// 	data[i]=j;
		// }

		//statNbVoix(data);
		//data = remplacer(data);
		//afficherDeroule(data,1);
		//statProbaVoixDebut(data,3);
		//afficherFugues(data);
		//afficherElemFugue(data);
		//affichageNbTransition(data);
		//affichageProbaTransition(data);
		//affichageNbSuperposition(data);
		//affichageProbaSuperposition(data);

		vector<string> elem = {"S","CS","Cad","Ped","Var","?","FIN"};
		vector<vector<float> > trans = {{0.000,0.462,0.077,0.000,0.298,0.163,0.000},
										{0.743,0.000,0.048,0.000,0.095,0.114,0.000},
										{0.000,0.000,0.000,0.553,0.000,0.000,0.447},
										{0.000,0.000,0.000,0.000,0.000,0.000,1.000},
										{0.630,0.123,0.041,0.000,0.000,0.205,0.000},
										{0.000,0.000,1.000,0.000,0.000,0.000,0.000}};

		int longueurMin = 5;
		vector<int> entreeSujet = choixEntreeSujet();
		vector<vector<string> > F = fugue(choixEntreeSujet(),elem,trans,longueurMin);
		afficherFugue(F);

	}
	return 0;
}
