#include <iostream>
#include <fstream> // Lire les fichiers
#include <string>
#include <iomanip> // std::setw pour un affichage propre
#include <vector>
#include <map>

using namespace std;

struct element{
	string nom; // Nom de l'élément de la fugue
	float longueur; // Longueur de l'élément
	float décallage; // Décallage du début de l'élément par rapport au début de la mesure
	map<float,char> instances; // Instance(s) de l'élément et voix sur laquelle il se trouve
};

// Afficher un vector
void afficher(vector<string> T){
	for(int i=0; i<T.size(); i++){
		cout<<i<<") '"<<T[i]<<"'"<<endl;
	}
}

// Afficher les données
void afficherData(vector<vector<element> > data){
	for(int i=1; i<data.size(); i++){
		cout<<"Fugue n°"<<i<<endl;
		for(int j=1; j<data[i].size(); j++){
			cout<<data[i][j].nom;
			if(data[i][j].longueur!=0.0){
				cout<<" (l="<<data[i][j].longueur<<" | dec="<<data[i][j].décallage<<")";
			}
			cout<<" :";
			map<float,char>::iterator it;
			for(it = (data[i][j].instances).begin(); it != (data[i][j].instances).end(); it++){
			    cout<<" ("<<it->first<<","<<it->second<<")";
			}
			cout<<endl;
		}
		cout<<endl;
		if(i==data.size()-1){
			cout<<endl;
		}
	}
}

vector<string> split(string str, string token){
    vector<string>result;
    while(str.size()){
        int index = str.find(token);
        if(index!=string::npos){
            result.push_back(str.substr(0,index));
            str = str.substr(index+token.size());
            if(str.size()==0)result.push_back(str);
        }else{
            result.push_back(str);
            str = "";
        }
    }
    return result;
}

// Vérifie si un motif correspond au début de la ligne
bool debutLigne(string ligne, string motif){
	bool resultat=true;
	int i=0;
	while(resultat && i<motif.length()){
		resultat=(ligne[i]==motif[i]);
		i++;
	}
	return resultat;
}

// Main
int main(int argc, char** argv)
{
	if(argc!=1){
		cout<<"ERREUR : Il faut 0 argument !"<<endl;
	}
	else{
		ifstream monFlux("fugues/fugues.ref");
		string ligne;
		string elem="";
		int numeroLigne=0;
		int numeroFugue=0;
		int numeroLigneDansFugue=0;
		vector<vector<element> > data;
		vector<element> fugue;
		element e;
		map<float,char> m;
		if(monFlux){
		    while(getline(monFlux, ligne) && numeroFugue<=24){ // Tant qu'on n'est pas à la fin, on lit
		    	if(ligne[0]!='#' && ligne.length()!=0){
		    		if(debutLigne(ligne,"==== w")){ // Début fugue Bach
		    			// Vider et initialiser
		    			if(!m.empty()){
			    			e.instances=m;
			    			m.clear();
			    			fugue.push_back(e);
			    		}
			    		data.push_back(fugue);
			    		fugue.clear();

		    			numeroFugue++;
		    			numeroLigneDansFugue++;
		    			//cout<<"Fugue n°"<<numeroFugue<<endl;
		    		}
		    		else if(numeroLigneDansFugue==1){ // Tonalité de la fugue
		    			numeroLigneDansFugue++;
		    		}
		    		else if(debutLigne(ligne,"  == ")){ // Élement de la fugue
		    			// Vider et initialiser
		    			if(!m.empty()){
			    			e.instances=m;
			    			m.clear();
			    			fugue.push_back(e);
			    		}

		    			vector<string> mots = split(ligne," ");
		    			e.nom = mots[3];
		    			if(mots[3]=="cadences" || mots[3]=="pedals"){
		    				e.longueur = 0.0; // Ces éléments n'ont pas de longueurs définies
		    			}
		    			else{
			    			if(mots[4]=="[length"){ // Prise en compte de la longueur de l'élément
			    				e.longueur = stof(mots[5]);
			    			}
			    			if(mots[6]=="start"){ // Prise en compte du décallage
			    				string fact = mots[7].substr(0,mots[7].size()-1);
			    				vector<string> composantes = split(fact,"/"); // Transformation d'une fraction en flottant
			    				if(composantes.size()==1){
			    					e.décallage=stof(composantes[0]);
			    				}
			    				else{
			    					e.décallage = stof(composantes[0])/stof(composantes[1]);
			    				}
			    			}
		    			}
		    		}
		    		else if(debutLigne(ligne,"        ")){
		    			vector<string> instances = split(ligne," ");
		    			char nomVoix = instances[8][0];
		    			for(int i=0; i<instances.size(); i++){
		    				// Nettoyer les virgules
		    				if(instances[i].back()==','){
		    					instances[i]=instances[i].substr(0,instances[i].size()-1);
		    				}
		    			}
		    			// Ajouter à la map m ce qui n'est ni dans des crochets ni des parenthèses
		    			bool crochet=false;
		    			bool parenthese=false;
		    			for(int i=10; i<instances.size(); i++){
		    				if(instances[i].front()=='('){
		    					parenthese=true;
		    				}
		    				if(instances[i].front()=='['){
		    					crochet=true;
		    				}
		    				if(!parenthese && !crochet){

		    					m[stof(instances[i])]=nomVoix;
		    					//cout<<"("<<instances[i]<<","<<nomVoix<<")"<<endl;
		    				}
		    				if(instances[i].back()==')'){
								parenthese=false;
		    				}
		    				if(instances[i].back()==']'){
								crochet=false;
		    				}
		    			}
		    			//afficher(instances);
		    		}
		    		else{
		    			//cout<<"ERREUR - "<< ligne<<endl;
		    		}
		    		//cout<<ligne<<endl;
		    	}
		    	numeroLigne++;
			}
			// Vider et initialiser
			if(!m.empty()){
    			e.instances=m;
    			m.clear();
    			fugue.push_back(e);
    		}
			data.push_back(fugue);
			fugue.clear();
		}
		else{
		    cout << "ERREUR : Impossible d'ouvrir le fichier en lecture." << endl;
		}

		afficherData(data);
	}
	return 0;
}