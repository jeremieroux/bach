import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.jfugue.theory.Key;
import org.jfugue.theory.Note;
import org.jfugue.theory.Scale;

public class Fugue {
	private String sujet;
	private ArrayList<Note> noteSujet;
	private ArrayList<Voix> voix;
	private static final Integer[] gammeMajeure = {0,2,4,5,7,9,11};
	private static final Integer[] gammeMineure = {0,2,3,5,7,8,11}; // Mineur harmonique
	private static final Integer[] intervalle = {3,4,5,8};
	private static final Integer[] noteSuivante = {-1,2,-2,3,4,-4,-3,1,5,-5,7,-7,6,-6,0};
	private Key tonalite;
	private int transpositionSujet;
	
	public Fugue(String sujet, int nbVoix, int transpositionSujet, String tonalite) {
		this.sujet = sujet;
		this.noteSujet = new Voix(this.sujet).getNotes();
		this.transpositionSujet=transpositionSujet;
		this.tonalite = new Key(tonalite);
		
		// Création des voix vides
		this.voix = new ArrayList<Voix>();
		for(int i=0; i<nbVoix; i++) {
			this.voix.add(new Voix());
		}
		
		// Ajout du sujet sur la première voix
		this.voix.get(0).add(this.noteSujet);
		
		ajouterVide(1,this.voix.get(0).duree());
		reponse(1);
		complementaire(this.noteSujet,0);
		marcheHarmonique();
		
	}
	
	public ArrayList<Voix> getVoix(){
		return this.voix;
	}
	
	public String toString() {
		String res="";
		for (Voix v : this.voix) {
			res+=v.toString()+"\n";
		}
		return res;
	}
	
	public ArrayList<Note> transpose(ArrayList<Note> notes, Integer nb){
		ArrayList<Note> nouvellesNotes = new ArrayList<Note>();	
		for (Note n : notes) {
			if(n.isRest()) {
				nouvellesNotes.add(n);
			}
			else {
				nouvellesNotes.add(new Note(n.getValue()+nb,n.getDuration()));
			}
		}
		return nouvellesNotes;
			
	}
	
	public boolean memeHauteur(int h1, int h2) {
		return h1%12==h2%12;
	}
	
	public boolean estGamme(int h) {
		
		// Attribution du bon mode
		Integer[] mode;
		if(this.tonalite.getScale().equals(Scale.MINOR)) {
			mode = Fugue.gammeMineure;
		}
		else {
			mode = Fugue.gammeMajeure;
		}
		
		int ecart = this.tonalite.getRoot().getValue()+this.transpositionSujet;	
		for(int i=0; i<mode.length; i++) {
			if(memeHauteur(ecart+mode[i],h)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean intervalleValide(int h1, int h2) {
		int ecart = Math.abs(h1-h2);
		for(int i=0; i<Fugue.intervalle.length; i++) {
			if(memeHauteur(ecart,Fugue.intervalle[i])) {
				return true;
			}
		}
		return false;
	}
	
	public Integer hauteurValide(int hauteur, int idVoix) {
		int hauteurPrecedente = this.voix.get(idVoix).derniereNote().getValue();
		for(int i=0; i<Fugue.noteSuivante.length; i++) {
			int h=hauteurPrecedente+Fugue.noteSuivante[i];
			if(estGamme(h) && intervalleValide(h,hauteur) && h<hauteur && !memeHauteur(h,this.voix.get(idVoix).avantDerniereNote().getValue()) && hauteur-h<12){
				return h;
			}
		}
		return hauteur;
	}
	
	public void ajouter(int idVoix, ArrayList<Note> notes) {
		this.voix.get(idVoix).add(notes);
	}
	
	public void ajouter(int idVoix, Note note) {
		this.voix.get(idVoix).add(note);
	}
	
	public void ajouterVide(int idVoix, double duree) {
		this.voix.get(idVoix).add(new Note("R/"+duree));
	}
	
	public void reponse(int idVoix) {
		ajouter(idVoix,transpose(this.noteSujet,this.transpositionSujet));
	}
	
	public void complementaire(ArrayList<Note> motif, int idVoix) {
		double duree=0;
		Note mem = null;
		for (Note n : transpose(motif,this.transpositionSujet)) {	
			if(n.getDuration()<=0.25) {
				if(duree==0) {
					mem = n;
				}
				duree+=n.getDuration();
			}
			else if(duree==0){
				int rand = (int) (Math.random()*5);
				if(rand==0) {
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/2));
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/2));
				}
				else if(rand==1){
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/4));
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/4));
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/4));
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/4));
				}
				else if(rand==2){
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/2));
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/4));
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/4));
				}
				else if(rand==3){
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/4));
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/4));
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/2));
				}
				else{
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/4));
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/2));
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), n.getDuration()/4));
				}
			}
			else if(duree!=0){
				ajouter(idVoix,new Note(hauteurValide(mem.getValue(),idVoix), duree));
				if(n.getDuration()>=0.5){
					//System.out.println("OUI");
					double dureeRestante=n.getDuration();
					do {
						int rand = (int) (Math.random()*2);
						if(rand==0) {
							ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), 0.25));
							dureeRestante-=0.25;
						}
						else {
							ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), 0.125));
							dureeRestante-=0.125;
						}
					}while(dureeRestante>0);
					if(dureeRestante!=0) {
						retirerDerniere(idVoix);
						ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), -dureeRestante));
					}
				}
				duree=0.;
			}
			else if(n.getDuration()>=0.5){
				System.out.println("OUI");
				double dureeRestante=n.getDuration();
				do {
					int rand = (int) (Math.random()*2);
					if(rand==0) {
						ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), 0.25));
						dureeRestante-=0.25;
					}
					else {
						ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), 0.125));
						dureeRestante-=0.125;
					}
				}while(dureeRestante>0);
				if(dureeRestante!=0) {
					retirerDerniere(idVoix);
					ajouter(idVoix,new Note(hauteurValide(n.getValue(),idVoix), -dureeRestante));
				}
			}
			else {
				System.out.println("Cas non traité");
			}
		}
		if(duree!=0) {
			//Note d = this.voix.get(idVoix).derniereNote();
			ajouter(idVoix,new Note(hauteurValide(mem.getValue(),idVoix), duree));
		}
	}
	
	public void marcheHarmonique() {
		
		ArrayList<Integer> ordreVoix = new ArrayList<Integer>();
		for(int i=0; i<this.voix.size()-1; i++) {
			ordreVoix.add(i);
		}
		Collections.shuffle(ordreVoix);
		
		
		int sens = (int) (Math.random()*2);
		if(sens==0) {
			sens=-1;
		}
		
		ArrayList<Note> motifMarche1 = new ArrayList<Note>();
		
		float somme=0;
		while(somme!=1) {
			int rand;
			if(somme==0.75) {
				rand = (int) (Math.random()*2);
			}
			else{
				rand = (int) (Math.random()*3);
			}
			
			int Hderniere;
			Hderniere = voix.get(ordreVoix.get(0)).derniereNote().getValue();
			int h = hauteurValide(Hderniere,ordreVoix.get(0));
			if(rand==0){
				ajouter(ordreVoix.get(0),new Note(h, 0.25));
				motifMarche1.add(new Note(h, 0.25));
				somme+=0.25;
			}
			else if(rand==1){
				ajouter(ordreVoix.get(0),new Note(h, 0.125));
				motifMarche1.add(new Note(h, 0.125));
				Hderniere = voix.get(ordreVoix.get(0)).derniereNote().getValue();
				h = hauteurValide(Hderniere,ordreVoix.get(0));
				ajouter(ordreVoix.get(0),new Note(h, 0.125));
				motifMarche1.add(new Note(h, 0.125));
				somme+=0.25;
			}
			else if(rand==2) {
				ajouter(ordreVoix.get(0),new Note(h, 0.5));
				motifMarche1.add(new Note(h, 0.5));
				somme+=0.5;
			}
		}
		//this.voix.get(ordreVoix.get(0)).add(motifMarche1);
		complementaire(motifMarche1,ordreVoix.get(1));
		
		ArrayList<Note> motifMarche2 = voix.get(ordreVoix.get(1)).derniereMesure();
		
		int nb = (int) (Math.random()*3)+2;
		for(int i=1; i<nb; i++) {
			this.transpositionSujet+=sens;
			for(Note n : motifMarche1) {
				ajouter(ordreVoix.get(0),new Note(n.getValue()+i*2*sens, n.getDuration()));
			}
			for(Note n : motifMarche2) {
				ajouter(ordreVoix.get(1),new Note(n.getValue()+i*2*sens, n.getDuration()));
			}
		}
		
		
	}

	public void retirerDerniere(int idVoix) {
		this.voix.get(idVoix).retirerDerniere();
	}
}
