import java.util.ArrayList;
import java.util.Collections;

import org.jfugue.theory.Note;

public class Voix {
	
	private ArrayList<Note> notes;
	
	public Voix() {
		this.notes = new ArrayList<Note>();
	}
	
	public Voix(String s) {
		ArrayList<Note> notes = new ArrayList<Note>();	
		String[] l = s.split(" ");
		
		for (String str_n : l) {
			  Note n = new Note(str_n);
			  notes.add(n);
		}
		
		this.notes = notes;
	}
	
	public Voix(ArrayList<Note> notes) {
		this.notes=notes;
	}
	
	public Voix(Note note) {
		this.notes=new ArrayList<Note>();
		this.notes.add(note);
	}
	
	public double duree() {
		double res = 0.0;
		for (Note n : this.notes) {
			res+=n.getDuration();
		}
		return res;
	}
	
	public void add(Note n) {
		this.notes.add(n);
	}
	
	public void add(ArrayList<Note> notes) {
		for (Note n : notes) {
			this.notes.add(n);
		}
	}
	
	public void retirerDerniere() {
		this.notes.remove(this.notes.size()-1);
	}
	
	public int hauteurDebut() {
		int h=0,i=0;
		while(h==0 && i<this.notes.size()) {
			if(!this.notes.get(i).isRest()) {
				h=this.notes.get(i).getValue();
			}
			i++;
		}
		return h;
	}
	
	public Note noteSelonTemps(double temps){
		double sum=0.;
		int i=0;
		double mem=0;
		while(sum<temps) {
			mem=sum;
			sum+=this.notes.get(i).getDuration();
			i++;
		}
		System.out.println("id="+i+" "+mem + "/" + temps+" -> "+this.notes.get(i));
		return this.notes.get(i);		
	}
	
	public ArrayList<Note> getNotes(){
		return this.notes;
	}
	
	public Note derniereNote() {
		return this.notes.get(this.notes.size()-1);
	}
	
	public Note avantDerniereNote() {
		return this.notes.get(this.notes.size()-2);
	}
	
	public ArrayList<Note> derniereMesure() {
		ArrayList<Note> n = new ArrayList<Note>();
		float t=0;
		int i=1;
		while(t<1.) {
			Note note = this.notes.get(this.notes.size()-i);
			n.add(note);
			t+=note.getDuration();
			i++;
		}
		Collections.reverse(n);
		return n;
	}
	
	public String toString() {
		String res="";
		for (Note n : this.notes) {
			res+=n.toString()+" ";
		}
		return res;
	}

}
