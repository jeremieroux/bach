import java.io.File;
import java.io.IOException;

import org.jfugue.midi.MidiFileManager;
import org.jfugue.pattern.Pattern;
import org.jfugue.player.Player;

public class Main {

	public static void main(String[] args) {
		//String sujet = "D5h A5h F5h D5h C#5h D5q E5q F5/0.625 G5i F5i E5i"; // art de la fugue
		//String sujet = "C5q D5q E5q C5q D5h D5q E5q F5h F5h E5h E5h C5q D5q E5q C5q D5h D5q E5q F5h G5h C5w"; // j'ai du bon tabac
		String sujet = "C5q C5q G5q G5q A5q A5q G5h F5q F5q E5q E5q D5i C5i D5q C5h"; // a vous dirais-je maman
		Fugue f = new Fugue(sujet,3,7,"Cmaj");
		System.out.println(f);
		ecrireMidi("ah_vous_dirais_je_maman.mid",f,false);
    }
	
	// Ecriture d'une simple mélodie dans un fichier MIDI
	static void ecrireMidi(String nomFichier, Pattern musique, boolean jouer) {
		try {
			File fichier = new File(nomFichier);
			if(jouer) {
				Player player = new Player();
				player.play(musique);
			}
            MidiFileManager.savePatternToMidi(musique, fichier);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
	}
	
	// Ecriture d'une fugue dans un fichier MIDI
	static void ecrireMidi(String nomFichier, Fugue f, boolean jouer) {
		try {
			File fichier = new File(nomFichier);
			String res = "";
			int i=0;
			for(Voix v : f.getVoix()) {
				//int i = (f.getVoix().indexOf(v)+1)%2; // Inversion des voix
				res+="V"+i+" "+v.toString()+" ";
				i++;
			}
			Pattern musique = new Pattern(res); 
			if(jouer) {
				Player player = new Player();
				player.play(musique);
			}
            MidiFileManager.savePatternToMidi(musique, fichier);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
	}
}
