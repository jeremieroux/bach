
#include "MidiFile.h"
#include <iostream>
#include <map>
#include <sstream>

using namespace std;
using namespace smf;

map<string, int> Tnote = {{"C",0},{"C+",1},{"D-",1},{"D",2},{"D+",3},{"E-",3},{"E",4},{"F",5},{"F+",6},{"G-",6},{"G",7},{"G+",8},{"A-",8},{"A",9},{"A+",10},{"B-",10},{"B",11}};
map<string, int> Toctave = {{"0",12},{"1",24},{"2",36},{"3",48},{"4",60},{"5",72},{"6",84},{"7",96}};

struct Note{
	int note;
	int rythme;
};

string toString(char c){
	string s = "";
	return s+=c;
}

string toString(char c1, char c2){
	string s = "";
	s+=c1;
	return s+=c2;
}

int toInt(char c, char d, char u){
	int c1 = c - '0';
	int d1 = d - '0';
	int u1 = u - '0';
	return c1*100 + d1*10 + u1;
}

vector<Note> lire(string s){
	vector<Note> musique;
	size_t pos = 0;
	string note;
	while ((pos = s.find(" ")) != string::npos) {
		note = s.substr(0, pos);
		Note n;

		if(note[2]=='_'){
			n.note=Tnote[toString(note[0])]+Toctave[toString(note[1])];
		}
		else if(note[3]=='_'){
			n.note=Tnote[toString(note[0],note[1])] + Toctave[toString(note[2])];
		}
		else{
			cout << "Note non reconnue : "<<note << endl;
		}

		if(note[note.size()-2]=='_'){
			n.rythme=toInt('0', '0', note[note.size()-1]);
		}
		else if(note[note.size()-3]=='_'){
			n.rythme=toInt('0', note[note.size()-2], note[note.size()-1]);
		}
		else if(note[note.size()-4]=='_'){
			n.rythme=toInt(note[note.size()-3], note[note.size()-2], note[note.size()-1]);
		}
		else{
			cout << "Rythme non reconnu : "<<note << endl;
		}
		cout<<"Ecrire note : "<< note << endl;
		musique.push_back(n);
		s.erase(0, pos+1);
	}
	return musique;
}

vector<Note> transposition(vector<Note> melodie, int ecart){
	vector<Note> transposition;
	for(unsigned int i=0; i<=melodie.size(); i++){
		Note n;
		n.note=melodie[i].note + ecart;
		n.rythme=melodie[i].rythme;
		transposition.push_back(n);
	}
	return transposition;
}

///////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {
	MidiFile outputfile;        // create an empty MIDI file with one track
	outputfile.absoluteTicks();  // time information stored as absolute time
	                           // (will be coverted to delta time when written)
	outputfile.addTrack(2);     // Add another two tracks to the MIDI file
	vector<uchar> midievent;     // temporary storage for MIDI events
	midievent.resize(3);        // set the size of the array to 3 bytes
	int tpq = 600;              // default value in MIDI file is 48
	outputfile.setTicksPerQuarterNote(tpq);

	vector<Note> sujet = lire("C5_1 C5_1 C5_1 D5_1 E5_2 D5_2 C5_1 E5_1 D5_1 D5_1 C5_4 FIN");
	vector<Note> sujetTranspose = transposition(sujet, -4);

	// Voix 1
	int actiontime = 0;      // temporary storage for MIDI event time
	midievent[2] = 64;       // store attack/release velocity for note command
	for(unsigned int i=0; i<=sujet.size(); i++){
		midievent[0] = 0x90;     // store a note on command (MIDI channel 1)
		midievent[1] = sujet[i].note;
		outputfile.addEvent(1, actiontime, midievent);
		actiontime += tpq * sujet[i].rythme;
		midievent[0] = 0x80;     // store a note on command (MIDI channel 1)
		outputfile.addEvent(1, actiontime, midievent);
	}

	
	// Voix 2
	actiontime = 0;          // reset time for beginning of file
	midievent[2] = 64;
	for(unsigned int i=0; i<=sujetTranspose.size(); i++){
		midievent[0] = 0x90;     // store a note on command (MIDI channel 1)
		midievent[1] = sujetTranspose[i].note;
		outputfile.addEvent(2, actiontime, midievent);
		actiontime += tpq * sujetTranspose[i].rythme;
		midievent[0] = 0x80;     // store a note on command (MIDI channel 1)
		outputfile.addEvent(2, actiontime, midievent);
	}

	outputfile.sortTracks();         // make sure data is in correct order
	outputfile.write("musique/fugue.mid"); // write Standard MIDI File twinkle.mid
	return 0;
}


/*  FUNCTIONS available in the MidiFile class:

void absoluteTime(void);
   Set the time information to absolute time.
int addEvent(int aTrack, int aTime, vector<uchar>& midiData);
   Add an event to the end of a MIDI track.
int addTrack(void);
   Add an empty track to the MIDI file.
int addTrack(int count);
   Add a number of empty tracks to the MIDI file.
void deltaTime(void);
   Set the time information to delta time.
void deleteTrack(int aTrack);
   remove a track from the MIDI file.
void erase(void);
   Empty the contents of the MIDI file, leaving one track with no data.
MFEvent& getEvent(int aTrack, int anIndex);
   Return a MIDI event from the Track.
int getTimeState(void);
   Indicates if the timestate is TIME_STATE_ABSOLUTE or TIME_STATE_DELTA.
int getTrackState(void);
   Indicates if the tracks are being processed as multiple tracks or
   as a single track.
int getTicksPerQuarterNote(void);
   Returns the ticks per quarter note value from the MIDI file.
int getTrackCount(void);
   Returns the number of tracks in the MIDI file.
int getNumTracks(void);
   Alias for getTrackCount();
int getNumEvents(int aTrack);
   Return the number of events present in the given track.
void joinTracks(void);
   Merge all tracks together into one track.  This function is reversable,
   unlike mergeTracks().
void mergeTracks(int aTrack1, int aTrack2);
   Combine the two tracks into a single track stored in Track1.  Track2
   is deleted.
int read(char* aFile);
   Read the contents of a MIDI file into the MidiFile class data structure
void setTicksPerQuarterNote    (int ticks);
   Set the MIDI file's ticks per quarter note information
void sortTrack(vector<MFEvent>& trackData);
   If in absolute time, sort particular track into correct time order.
void sortTracks(void);
   If in absolute time, sort tracks into correct time order.

*/



